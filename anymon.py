#!/usr/bin/env python2

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import anymon

if __name__ == "__main__":
	arguments = anymon.settings.Arguments().arguments

	settings = anymon.settings.Settings()
	settings.read(arguments.config)

	daemon = anymon.daemon.Daemon(settings, arguments.pidfile)
	daemon.run()
