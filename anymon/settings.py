from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import ConfigParser as configparser
import anymon
import argparse

class Settings(configparser.RawConfigParser, object):
	defaultsection = 'default'

	def read(self, configfile):
		super(Settings, self).read(configfile)
		self.__buildchecklist__()

	def __buildchecklist__(self):
		sections = self.sections()

		#collect all checks
		self.checklist=[]
		for section in sections:
			if section == self.defaultsection:
				check = anymon.checks.BaseCheck
			else:
				check = anymon.checks.BaseCheck.subclassbycheck(self.get(section,'check'))(section)
			check.setattrs(self.items(section))
			if check != anymon.checks.BaseCheck:
				self.checklist.append(check)

class Arguments(argparse.ArgumentParser):
	def __init__(self):
		super(Arguments, self).__init__(description='Monitor anycast services')
		self.add_argument('--pidfile', '-p', required=True, help='PID file location.')
		self.add_argument('--config', '-c', required=True, help='Config file location.')
		self.arguments  = self.parse_args()
