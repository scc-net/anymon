from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import multiprocessing
import traceback
import time
import os
import anymon

class Worker(multiprocessing.Process):
	def __init__(self, check, ppid):
		super(Worker, self).__init__()
		self.check = check
		self.ppid = ppid
		self.exit = multiprocessing.Event()

	def run(self):
		iface = anymon.network.Interface(self.check.interface)
		failcount = 0
	        while not self.exit.is_set():
			try:
				if self.ppid != os.getppid():
					#log?
					break
				if iface.isdown():
					failcount = 0
					time.sleep(int(self.check.gracetime))
					continue
				try:
					self.check.docheck()
				except:
					failcount = failcount + 1
				if failcount >= int(self.check.attempts):
					iface.down()
					continue
				time.sleep(int(self.check.interval))
			except:
				e = sys.exc_info()[0]
				print('Caught exception in worker thread %s:' % self.check.name)
				traceback.print_exc()
				print()
				raise e

	def shutdown(self):
		#log?
		self.exit.set()
