from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pprint
import pyroute2

class Interface(object):
	def __init__(self,name):
		self.name = name
		self.ip = pyroute2.IPRoute()
		self.index = self.ip.link_lookup(ifname=self.name)[0]
	def getstate(self):
		return self.ip.get_links([self.index])[0].get_attr('IFLA_OPERSTATE')
	def isup(self):
		return self.getstate() != 'DOWN'
	def isdown(self):
		return self.getstate() == 'DOWN'
	def down(self):
		self.ip.link('set', index=self.index, state='down')
