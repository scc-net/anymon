from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from combomethod import combomethod
import inspect
import socket
import ntplib
import sys
import dns.resolver

class BaseCheck(object):
	attempts = 3
	interval = 10
	timeout = 1
	gracetime = 10
	interface = None
	ip = None
	def __init__(self, name):
		self.name = name
	@classmethod
	def subclassbycheck(cls, check):
		for subclass in cls.__subclasses__():
			if subclass.check == check:
				return subclass
		else:
			raise BaseException('FIXME: check %s is unknown' % check)
	@combomethod
	def setattr(me, var, value):
		if hasattr(me,var):
			setattr(me,var,value)
		else:
			if inspect.isclass(me):
				fixme = "default section"
			else:
				fixme = "check %s in section %s" % (me.check,me.name)
			raise BaseException('FIXME: %s has no option %s' % (fixme,var))
	@combomethod
	def setattrs(me, options):
		for option in options:
			#don't set instance attribute check, because class attribute has the same value
			if option != 'check':
				me.setattr(*option)

class DNSCheck(BaseCheck):
	check = 'dns'
	rdtype = 'TXT'
	rdclass = 'CH'
	queryname = 'hostname.bind'
	value = socket.gethostname()

	def docheck(self):
		resolver = dns.resolver.Resolver()
		resolver.nameservers = [self.ip]
		resolver.lifetime = int(self.timeout)
		results = resolver.query(self.queryname, self.rdtype, self.rdclass)
		if not results and len(value):
			raise BaseException('FIXME: no result for check %s', (self.name))
		for result in results:
			if not self.value.strip().count(','):
				if str(result).strip('"') != self.value:
					raise BaseException('FIXME: not allowed result %s in check %s', (result, self.name))
			else:
				if not str(result).strip('"') in self.value.strip().split(','):
					raise BaseException('FIXME: not allowed result %s in check %s', (result, self.name))
		return True

class NTPCheck(BaseCheck):
	check = 'ntp'

	def docheck(self):
		ntpclient = ntplib.NTPClient()
		response = ntpclient.request(self.ip, self.timeout)
		#print(response.delay)
