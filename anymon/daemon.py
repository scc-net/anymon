from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import anymon
import daemon.pidlockfile
import lockfile
import sys
import multiprocessing
import time
import signal
import os

class Daemon(object):
	def __init__(self, settings, pidfile):
		self.settings = settings
		self.pidfile = pidfile
		self.context = daemon.DaemonContext(stdout=sys.stdout, stderr=sys.stderr, pidfile=daemon.pidlockfile.TimeoutPIDLockFile(self.pidfile,1))
	def run(self):
		try:
			self.mainloop()
		except lockfile.LockTimeout:
			#log? stderr?
			print('FIXME: daemon already running')
	def mainloop(self):
		with self.context:
			processes = self.startprocesses()

			signal.signal(signal.SIGTERM, self.signal_term_handler(processes))

			while self.aliveporcesses(processes):
				time.sleep(1)

			self.stopprocesses(processes)
	def signal_term_handler(self, processes):
		def term_handler(signal, frame):
			self.stopprocesses(processes)
			sys.exit(0)
		return term_handler
	def startprocesses(self):
		processes = []
		for check in self.settings.checklist:
			process = anymon.worker.Worker(check,os.getpid())
			processes.append(process)
			process.start()
		return processes
	def stopprocesses(self, processes):
		for process in processes:
			process.shutdown()
	def aliveporcesses(self, processes):
		return all([process.is_alive() for process in processes])
